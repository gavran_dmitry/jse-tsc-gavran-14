package ru.tsc.gavran.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStatus, IHasStartDate {
}
